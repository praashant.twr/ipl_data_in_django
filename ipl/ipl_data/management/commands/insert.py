from django.core.management.base import BaseCommand
from ipl_data.models import Delivery, Match
import os


class Command(BaseCommand):
    def handle(self, *args, **kwargs):
        
        insert_count = Match.objects.from_csv(os.path.abspath('ipl_data/management/commands/matches.csv'))
        print(f'{insert_count} insert into match')
        insert_count = Delivery.objects.from_csv(os.path.abspath('ipl_data/management/commands/deliveries.csv'))
        print(f'{insert_count} insert into delivery')
        