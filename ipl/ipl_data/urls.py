from django.urls import path
from . import views

app_name = 'ipl'
urlpatterns=[
    path('', views.index, name='index'),
    path('matches/', views.get_matches),
    path('matches/<int:id>', views.get_matches_by_id),
    path('deliveries/', views.get_deliveries),
    path('deliveries/<int:id>', views.get_deliveries_by_id),
    path('matchsperseason/',views.matches_per_season, name='matchesperseason'),
    path('matchsperseason/chart',views.chart_mps, name='chartmps'),
    path('extraruns/',views.extra_runs_per_team, name='extrarunsperteam'),
    path('extraruns/chart', views.chart_erpt, name='charterpt'),
    path('matchesperyear/',views.matches_won_per_year, name='matcheswonperyear'),
    path('matchesperyear/chart', views.chart_mwpy, name='chartmwpy'),
    path('bowlereconomy/',views.top_economic_bowlers, name='topeconomicbowler'),
    path('bowlereconomy/chart', views.chart_teb, name='chartteb'),
    path('match_list/', views.MatchList.as_view()),
    path('delivery_list/', views.DeliveryList.as_view()),
]