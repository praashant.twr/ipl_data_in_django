from .models import Delivery, Match
from django.shortcuts import render
from django.db.models import Count, Sum, FloatField, Max, Case, When, F
from django.db.models.functions import Cast
from django.http import JsonResponse, HttpResponse
from django.core import serializers
from django.views.decorators.cache import cache_page
from django.conf import settings
from rest_framework.response import Response
from rest_framework import status
from rest_framework.views import APIView
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.parsers import JSONParser
from django.core.cache.backends.base import DEFAULT_TIMEOUT
from django.views.decorators.csrf import csrf_exempt
from ipl_data.serializers import MatchSerializer, DeliverySerializer
from django.core.paginator import Paginator
from rest_framework import generics




CACHE_TTL = getattr(settings, 'CACHE_TTL', DEFAULT_TIMEOUT)

# Create your views here.
def make_chart(dict, title, limit=-1):
    chart = {
        'chart': {
            'type':'column'
        },
        'title': {
            'text': title
        },
        'xAxis': {
            'categories': list(dict.keys())[:limit]
        },
        'series': [
            {
                'name': 'matches Played',
                'data': list(dict.values())[:limit]
            }
        ]
    }
    return chart

@cache_page(CACHE_TTL)
def index(request):
    return render(request, 'index.html')

@cache_page(CACHE_TTL)
def matches_per_season(request):
    return  render(request, 'matchesperseason.html')

@cache_page(CACHE_TTL)
def extra_runs_per_team(request):
    return render(request, 'extrarunsperteam.html')

@cache_page(CACHE_TTL)
def top_economic_bowlers(request):
    return render(request, 'topeconomicbowler.html')

@cache_page(CACHE_TTL)
def matches_won_per_year(request):
    return render(request, 'matcheswonperyear.html')

@cache_page(CACHE_TTL)
def chart_mps(request):
    matches = Match.objects.values('season').annotate(Count('date'))
    matches_per_season_ = {}
    for match in matches:
        matches_per_season_[match['season']] = match['date__count']
    chart = make_chart(matches_per_season_, 'Matches Played Per Season')
    return JsonResponse(chart) 

@cache_page(CACHE_TTL)
def chart_erpt(request):
    r = Delivery.objects.values('bowling_team').filter(match_id__season=2016).annotate(extra_runs = Sum('extra_runs')).order_by('extra_runs')

    extra_runs_per_team_ = {}
    for li in r:
        extra_runs_per_team_[li['bowling_team']] = li['extra_runs']
    chart = make_chart(extra_runs_per_team_, 'Extra Runs Per Team In 2016')
    return JsonResponse(chart)


@cache_page(CACHE_TTL)
def chart_teb(request):
    r = Delivery.objects.filter(match_id__season = 2015, is_super_over = False).\
                    values('bowler').annotate(runs = Sum('batsman_runs') + Sum('wide_runs') + \
                    Sum('noball_runs')).annotate(balls = Count('ball')- \
                    Count(Case(When(noball_runs__gt = 0, then = 1)))-\
                    Count(Case(When(wide_runs__gt = 0, then = 1)))).\
                    annotate(economy = Cast((F('runs')/(F('balls')/6.0)),\
                    FloatField())).order_by('economy')[:10]
    top_economic_bowlers_ = {}
    for li in r:
        top_economic_bowlers_[li['bowler']] = li['economy']
    chart = make_chart(top_economic_bowlers_, 'Top Economic Bowlers of 2015', 10)
    return JsonResponse(chart)

@cache_page(CACHE_TTL)
def chart_mwpy(request):
    m = Match.objects.values('winner', 'season').annotate(Count('season'))
    winning_record = {}
    q = Match.objects.values('season').distinct()
    seasons = []
    for a in q:
        seasons.append(a['season'])
    seasons.sort()

    for a in m:
        if a['winner'] not in winning_record:
                winning_record[a['winner']] = {a['season']: a['season__count']}
        else:
                winning_record[a['winner']][a['season']] = a['season__count']
    
    for wins in winning_record.values():
        for season in seasons:
            if season not in wins:
                wins[season] = 0
    
    chart_content = []
    
    for team, wins in winning_record.items():
        team_with_wins = {}
        wins_of_team = []
        for win in sorted(wins):
            wins_of_team.append(wins[win])
        team_with_wins['name'] = team
        team_with_wins['data'] = wins_of_team
        chart_content.append(team_with_wins)

    chart = {
        'chart': {
            'type': 'column'
        },
        'title': {
            'text': 'Matches Played Per Team'
        },
        'xAxis': {
            'categories': seasons
        },
        'yAxis': {
            'min': 0,
            'title': {
            'text': 'Matches Played'
            }
        },
        'legend': {
            'reversed': 'true'
        },
        'plotOptions': {
            'series': {
                'stacking': 'normal'
            }
        },
        'series': chart_content
    }
    return JsonResponse(chart)

@api_view(['GET', 'POST'])
def get_matches(request):
    if request.method == 'GET':
        limit = request.GET.get('limit') or 20
        offset = request.GET.get('offset') or 0
        matches = Match.objects.all()[int(offset): int(limit)+int(offset)]
        serializer = MatchSerializer(matches, many=True)
        return Response(serializer.data)
    
    elif request.method == 'POST':
        match_data = eval(request.body.decode('utf-8'))
        serializer = MatchSerializer(data=match_data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    
@api_view(['GET', 'POST'])
def get_deliveries(request):
    if request.method == 'GET':
        limit = request.GET.get('limit') or 120
        offset = request.GET.get('offset') or 0
        deliveries = Delivery.objects.all()[int(offset): int(limit)+int(offset)]
        serializer = DeliverySerializer(deliveries, many=True)
        return Response(serializer.data)

    elif request.method == 'POST':
        deliveries_data = eval(request.body.decode('utf-8'))
        serializer = DeliverySerializer(data=deliveries_data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        

@api_view(['GET', 'PUT', 'DELETE'])
def get_matches_by_id(request, id):
    try:
        match = Match.objects.get(pk=id)
    except Match.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = MatchSerializer(match)
        return Response(serializer.data)
        
    elif request.method == 'PUT':
        match_data = JSONParser().parse(request)
        serializer = MatchSerializer(match, data=match_data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_404_BAD_REQUEST)

    elif request.method == 'DELETE':
        match.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

@api_view(['GET', 'PUT', 'DELETE'])
def get_deliveries_by_id(request, id):
    try:
        delivery = Delivery.objects.get(pk=id)
    except Delivery.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = DeliverySerializer(delivery)
        return Response(serializer.data)

    elif request.method == 'PUT':
        serializer = DeliverySerializer(delivery, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_404_BAD_REQUEST)


    elif request.method == 'DELETE':
        delivery.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


#REST APIs
class MatchList(generics.ListCreateAPIView):
    queryset = Match.objects.all()
    serializer_class = MatchSerializer

class DeliveryList(generics.ListCreateAPIView):
    queryset = Delivery.objects.all()
    serializer_class = DeliverySerializer
























































































# def deliveries_json(deliveries):
#     deliveries_data = {}
#     for delivery in deliveries:
#         data = {}
#         data['inning'] = delivery.inning
#         data['batting_team'] = delivery.batting_team
#         data['bowling_team'] = delivery.bowling_team
#         data['over'] = delivery.over
#         data['ball'] = delivery.ball
#         data['batsman'] = delivery.batsman
#         data['non_striker'] = delivery.non_striker
#         data['bowler'] = delivery.bowler
#         data['is_super_over'] = delivery.is_super_over
#         data['wide_runs'] = delivery.wide_runs
#         data['bye_runs'] = delivery.bye_runs
#         data['legbye_runs'] = delivery.legbye_runs
#         data['noball_runs'] = delivery.noball_runs
#         data['penalty_runs'] = delivery.penalty_runs
#         data['batsman_runs'] = delivery.batsman_runs
#         data['extra_runs'] = delivery.extra_runs
#         data['total_runs'] = delivery.total_runs
#         data['player_dismissed'] = delivery.player_dismissed
#         data['dismissal_kind'] = delivery.dismissal_kind
#         data['fielder'] = delivery.fielder
        
#         deliveries_data[delivery.id] = data
#     return deliveries_data














# data = eval(request.body.decode('utf-8'))
#         delivery = Delivery(
#             id = Delivery.objects.aggregate(Max('id'))['id__max'] + 1,
#             inning=data['inning'],
#             batting_team=data['batting_team'],
#             bowling_team=data['bowling_team'],
#             over=data['over'],
#             ball=data['ball'],
#             batsman=data['batsman'],
#             non_striker=data['non_striker'],
#             bowler=data['bowler'],
#             is_super_over=data['is_super_over'],
#             wide_runs=data['wide_runs'],
#             bye_runs=data['bye_runs'],
#             legbye_runs=data['legbye_runs'],
#             noball_runs=data['noball_runs'],
#             penalty_runs=data['penalty_runs'],
#             batsman_runs=data['batsman_runs'],
#             extra_runs=data['extra_runs'],
#             total_runs=data['total_runs'],
#             player_dismissed=data['player_dismissed'],
#             dismissal_kind=data['dismissal_kind'],
#             fielder=data['fielder']
#         )
#         delivery.save()
#         return HttpResponse(f'{data} with ID = {delivery.id} saved to Delivery Model')









# def match_json(matches):
#     matches_data = {}
#     for m in range(len(matches)):
#         data = {}
#         data['season'] = matches[m].season
#         data['city'] = matches[m].city
#         data['date'] = matches[m].date
#         data['team1'] = matches[m].team1
#         data['team2'] = matches[m].team2
#         data['toss_winner'] = matches[m].toss_winner
#         data['toss_decision'] = matches[m].toss_decision
#         data['result'] = matches[m].result
#         data['dl_applied'] = matches[m].dl_applied
#         data['winner'] = matches[m].winner
#         data['win_by_runs'] = matches[m].win_by_runs
#         data['win_by_wickets'] = matches[m].win_by_wickets
#         data['player_of_match'] = matches[m].player_of_match
#         data['venue'] = matches[m].venue
#         data['umpire1'] = matches[m].umpire1
#         data['umpire2'] = matches[m].umpire2
#         data['umpire3'] = matches[m].umpire3
    
#         matches_data[matches[m].id] = data
#     return matches_data













# if request.method == 'POST':
    #     data = eval(request.body.decode('utf-8'))
    #     match = Match(
    #         id = Match.objects.aggregate(Max('id'))['id__max'] + 1,
    #         season = data['season'],
    #         city = data['city'],
    #         date = data['date'], 
    #         team1 = data['team1'],
    #         team2 = data['team2'],
    #         toss_winner = data['toss_winner'],
    #         toss_decision = data['toss_decision'],
    #         result = data['result'],
    #         dl_applied = data['dl_applied'],
    #         winner = data['winner'],
    #         win_by_runs = data['win_by_runs'],
    #         win_by_wickets = data['win_by_wickets'],
    #         player_of_match = data['player_of_match'],
    #         venue = data['venue'],
    #         umpire1 = data['umpire1'],
    #         umpire2 = data['umpire2'],
    #         umpire3 = data['umpire3']
    #     )
    #     match.save()
    #     return HttpResponse(f'DATA :{data} with ID = {match.id}  saved to Match Model')
    # matches = Match.objects.all()
    # matches_data = match_json(matches)
    # # return JsonResponse(matches_data)
    # return Response(matches_data)
