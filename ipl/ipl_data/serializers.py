from rest_framework import serializers
from ipl_data.models import Match, Delivery

class MatchSerializer(serializers.ModelSerializer):
    class Meta:
        model = Match
        fields = '__all__'

class DeliverySerializer(serializers.ModelSerializer):
    class Meta:
        model = Delivery
        fields = "__all__"


















































































# class MatchSerializer(serializers.Serializer):
#     season = serializers.IntegerField()
#     city = serializers.CharField(max_length = 50, allow_blank=True)
#     date = serializers.DateTimeField()    
#     team1 = serializers.CharField(max_length = 100)
#     team2 = serializers.CharField(max_length = 100)
#     toss_winner = serializers.CharField(max_length = 100)
#     toss_decision = serializers.CharField(max_length = 100)
#     result = serializers.CharField(max_length = 100)
#     dl_applied = serializers.IntegerField()
#     winner = serializers.CharField(max_length = 100, allow_blank=True)
#     win_by_runs = serializers.IntegerField()
#     win_by_wickets = serializers.IntegerField()
#     player_of_match = serializers.CharField(max_length = 100, allow_blank=True)
#     venue = serializers.CharField(max_length = 50)
#     umpire1 = serializers.CharField(max_length = 100, allow_blank=True)
#     umpire2 = serializers.CharField(max_length = 100, allow_blank=True)
#     umpire3 = serializers.CharField(max_length = 100, allow_blank=True)

#     def create(self, validated_data):
#         return Match.objects.create(**validated_data)

#     def update(self, instance, validated_data):
#         instance.season = validated_data.get('season', instance.season) 
#         instance.city = validated_data.get('city', instance.city) 
#         instance.date = validated_data.get('date', instance.date) 
#         instance.team1 = validated_data.get('team1', instance.team1)
#         instance.team2 = validated_data.get('team2', instance.team2)
#         instance.toss_winner = validated_data.get('toss_winner', instance.toss_winner)
#         instance.toss_decision = validated_data.get('toss_decision', instance.toss_decision)
#         instance.result = validated_data.get('result', instance.result)
#         instance.dl_applied = validated_data.get('dl_applied', instance.dl_applied)
#         instance.winner = validated_data.get('winner', instance.winner)
#         instance.win_by_runs = validated_data.get('win_by_runs', instance.win_by_runs)
#         instance.win_by_wickets = validated_data.get('win_by_wickets', instance.win_by_wickets)
#         instance.player_of_match = validated_data.get('player_of_match', instance.player_of_match)
#         instance.venue = validated_data.get('venue', instance.venue)
#         instance.umpire1 = validated_data.get('umpire1', instance.umpire1)
#         instance.umpire2 = validated_data.get('umpire2', instance.umpire2)
#         instance.umpire3 = validated_data.get('umpire3', instance.umpire3)

#         instance.save()
#         return instance



# class DeliverySerializer(serializers.Serializer):
    # match_id = serializers.IntegerField()
    # inning = serializers.IntegerField()
    # batting_team = serializers.CharField(max_length = 100)
    # bowling_team = serializers.CharField(max_length = 100)
    # over = serializers.IntegerField()
    # ball = serializers.IntegerField()
    # batsman = serializers.CharField(max_length = 100)
    # non_striker = serializers.CharField(max_length = 100)
    # bowler = serializers.CharField(max_length = 100)
    # is_super_over = serializers.IntegerField()
    # wide_runs = serializers.IntegerField()
    # bye_runs = serializers.IntegerField()
    # legbye_runs = serializers.IntegerField()
    # noball_runs = serializers.IntegerField()
    # penalty_runs = serializers.IntegerField()
    # batsman_runs = serializers.IntegerField()
    # extra_runs = serializers.IntegerField()
    # total_runs = serializers.IntegerField()
    # player_dismissed = serializers.CharField(max_length = 100, allow_blank=True)
    # dismissal_kind = serializers.CharField(max_length = 50, allow_blank=True)
    # fielder = serializers.CharField(max_length = 100, allow_blank=True)

    # def create(self, validated_data):
    #     return Delivery.objects.create(**validated_data)
    
    # def update(self, instance, validated_data):
    #     instance.match_id = validated_data.get('match_id', instance.match_id)
    #     instance.inning = validated_data.get('inning', instance.inning) 
    #     instance.batting_team = validated_data.get('batting_team', instance.batting_team)  
    #     instance.bowling_team = validated_data.get('bowling_team', instance.bowling_team)   
    #     instance.over = validated_data.get('over', instance.over) 
    #     instance.ball = validated_data.get('ball', instance.ball) 
    #     instance.batsman = validated_data.get('batsman', instance.batsman)  
    #     instance.non_striker = validated_data.get('non_striker', instance.non_striker)  
    #     instance.bowler = validated_data.get('bowler', instance.bowler)  
    #     instance.is_super_over = validated_data.get('is_super_over', instance.is_super_over) 
    #     instance.wide_runs = validated_data.get('wide_runs', instance.wide_runs) 
    #     instance.bye_runs = validated_data.get('bye_runs', instance.bye_runs) 
    #     instance.legbye_runs = validated_data.get('legbye_runs', instance.legbye_runs) 
    #     instance.noball_runs = validated_data.get('noball_runs', instance.noball_runs) 
    #     instance.penalty_runs = validated_data.get('penalty_runs', instance.penalty_runs) 
    #     instance.batsman_runs = validated_data.get('batsman_runs', instance.batsman_runs) 
    #     instance.extra_runs = validated_data.get('extra_runs', instance.extra_runs) 
    #     instance.total_runs = validated_data.get('total_runs', instance.total_runs) 
    #     instance.player_dismissed = validated_data.get('player_dismissed', instance.player_dismissed)  
    #     instance.dismissal_kind = validated_data.get('dismissal_kind', instance.dismissal_kind)  
    #     instance.fielder = validated_data.get('fielder', instance.fielder)  