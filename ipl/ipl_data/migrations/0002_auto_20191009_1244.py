# Generated by Django 2.2.6 on 2019-10-09 12:44

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('ipl_data', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='delivery',
            name='id',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, primary_key=True, serialize=False, to='ipl_data.Match'),
        ),
    ]
